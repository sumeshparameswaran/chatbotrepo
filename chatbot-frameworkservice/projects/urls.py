from django.contrib import admin
from django.urls import path
from .views import NewProjectViewSet, LoadProjectsByUserViewSet, DeleteProjectViewSet

urlpatterns = [
    path('newProject', NewProjectViewSet.as_view(), name="newProject"),
    path('loadProjectsByUser', LoadProjectsByUserViewSet.as_view(), name="loadProjectByUser"),
    path('deleteProject', DeleteProjectViewSet.as_view(), name="deleteProject")
]