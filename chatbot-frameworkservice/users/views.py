from rest_framework import viewsets
from .models import User
from .serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import viewsets, generics
from django.views.decorators.csrf import csrf_exempt

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class LoginViewSet(generics.GenericAPIView):
    serializer_class = UserSerializer

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        username = request.data["username"]
        details = User.objects.filter(username = username)
        return Response({"success":"success", "role": details[0].role, "id" : details[0].id})