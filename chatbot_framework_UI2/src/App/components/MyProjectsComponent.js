import React from "react";
import { FaTrash, FaPen } from "react-icons/fa";
import {
  Card,
  CardBody,
  CardText,
  CardLink,
  Row,
  Col,
  CardHeader,
  CardDeck,
} from "reactstrap";
import axios from "axios";
import Modalpopup from './util/ModalPopUp'
import { Redirect } from "react-router-dom";
import * as actions from "../../store/myproject";
import { connect } from "react-redux";


class MyProjects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalOpened : false,
      deltedproject : null,
    };

 }

  editProject = (e, project) => {
    e.preventDefault();
    console.log(project);
    let   data = {
      projectname: project.name,
      description: project.description,
      showComponent: false,
    } 
    this.props.history.push("/useCase");
    
    // this.props.history.state = data;
    //return <Redirect to="/default/newProject" />;
  };

  showPopDelete = (e,deleteProject)=> {
    this.setState(prevState => ({ modalOpened: !prevState.modalOpened }));
    this.setState( {deltedproject : deleteProject})
    
  }




  deleteProject = (e, deleteProject) => {
    e.preventDefault();
    console.log(deleteProject.id);
    this.setState(prevState => ({ modalOpened: !prevState.modalOpened }));
    this.setState( {deltedproject : null})
    this.props.projectData(deleteProject);
  };

  
  componentDidMount() {
    this.props.loadProjectbyuser();
  }

  hidePopUp =()=>{
    this.setState(prevState => ({ modalOpened: !prevState.modalOpened }));
    this.setState( {deltedproject : null})
    return true;
  }

  
  
  render() {
    return (
      
      <div className="container">
        <h3>My Projects</h3>
        <hr />
        <div style = {{width : '750px',overflowY :'scroll',height:'500px',marginLeft:'20px'}}>
         <div
          className="row"
          style={{
            display: "block",
            maxwidth: "1000px",
            textalign: "center",
            margin: "0 auto",
          }}
        >
           {this.props.myProjects ? this.props.myProjects.map((project) => {
            return (
              <div
                className="col"
                key={project.id}
                style={{
                  backgroundColor: "white",
                  display: "inline-block",
                  width: "300px",
                  margin: "0 25px 25px",
                  borderRadius: "5px",
                }}
              >
                <Card
                  key={project.name}
                  style={{
                    backgroundColor: "white",
                    borderRadius: "10px",
                  }}
                >
                  <CardHeader>welcome {project.name}</CardHeader>
                  <CardBody>
                    <CardText>{project.description}</CardText>
                    <CardLink>
                      <Row>
                        <Col>
                          <button
                            style={{
                              borderRadius: "0px",
                              outline: "0",
                              borderWidth: "0px",
                              backgroundColor: "white",
                            }}
                            onClick={(e) => this.editProject(e, project)}
                          >
                            <FaPen color="black"></FaPen>
                          </button>
                        </Col>
                        <Col>
                          <button
                            style={{
                              borderRadius: "0px",
                              outline: "0",
                              borderWidth: "0px",
                              backgroundColor: "white",
                            }}
                            onClick={(e) => this.showPopDelete(e, project)}
                          >
                            <FaTrash />
                          </button>
                        </Col>
                       
                      </Row>
                    </CardLink>
                    <CardDeck></CardDeck>
                  </CardBody>
                </Card>
              </div>
            );
          }):(<div></div>)} 
        </div>
        </div>
        <Modalpopup modalOpened = {this.state.modalOpened} hidePopUp = {this.hidePopUp} deleteProject = {(e)=>this.deleteProject(e,this.state.deltedproject)}></Modalpopup>
      </div> 
    );
  }
}

const mapStateToProps = (state) => {
  return {
    myProjects: state.projects,
    loading : state.loading,
    error : state.error,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    projectData: (deleteProject) =>
      dispatch(actions.deleteProject(deleteProject)),
      loadProjectbyuser:() =>
      dispatch(actions.loadProjectbyuser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyProjects);

